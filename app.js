const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config/init')
const path = require("path");
const morgan = require("morgan");
const fs = require("fs");
const passport = require('passport');

app.use(passport.initialize());
const accessLogStream = fs.createWriteStream(
    path.join(__dirname, "logs", "access.log"),
    { flags: "a" }
);
app.use(morgan("combined", { stream: accessLogStream }));

//routes

const librarianRoutes = require('./api/routes/librarian');
const bookRoutes = require('./api/routes/book');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.use(config.cors);

app.use('/librarian', librarianRoutes);
app.use('/book', bookRoutes);



module.exports = app;