const http = require('http');
const app = require('./app');
const db = require('./database');

require('dotenv').config()

const port = process.env.PORT;
const server = http.createServer(app);

server.listen(port, () => {
    console.log(`index is up on port ${port}!`);
});