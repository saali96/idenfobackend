const express = require('express');
const router = express();
const BookController = require('../controller/book.js');
const passport = require('passport');
const strategies = require("../../services/loginStrategies");
passport.use('librarian', strategies.JWTStrategyLibrarian);
module.exports = router;

/*
create api is use to create book
*/
router.post('/create', passport.authenticate("librarian", { session: false }), BookController.createBook);
/*
list api is use to list all the books
*/
router.get('/list', passport.authenticate("librarian", { session: false }), BookController.ListAllBooks);
/*
find api is use to list selected book
*/
router.get('/find', passport.authenticate("librarian", { session: false }), BookController.ListSpecificBook);
/*
checkout api is use to checkout selected book
*/
router.put('/checkout', passport.authenticate("librarian", { session: false }), BookController.checkoutBook);
/*
checkin api is use to checkin selected book
*/
router.put('/checkin', passport.authenticate("librarian", { session: false }), BookController.checkInBook);


