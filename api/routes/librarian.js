const express = require('express');
const router = express();
const librarianController = require('../controller/librarian.js');
const passport = require('passport');
const strategies = require("../../services/loginStrategies");
passport.use('user', strategies.JWTStrategyLibrarian);
module.exports = router;

/*
signup api is use to register mobile number
*/
router.post('/signup', librarianController.signupLibrarian);

/*
login api use to verify credentials and return token
*/
router.post('/login', librarianController.loginLibrarian);

