var Librarian = require('../../models/librarian.js');
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const strategies = require("../../services/loginStrategies");

module.exports = {
    /*  
 
    variables used:            

    email:email of the user
    password:password of the user        

    */
    signupLibrarian: async (req, res) => {
        var { email, password } = req.body;

        if (email == null || !email) {
            res.status(422).json({ success: false, message: 'Invalid email', data: [] });
            return
        }
        if (password == null || !password) {
            res.status(422).json({ success: false, message: 'Invalid password', data: [] });
            return
        }
        if (password.length < 8) {
            res.status(422).json({ success: false, message: 'Password should be atleast 8 character long', data: [] });
            return
        }
        email = email.toLowerCase();

        try {
            Librarian.findOne({ email }, (err, resp) => {
                if (err) {
                    return res.json({ status: err });
                }
                else if (resp) {
                    return res.status(409).send({ success: false, message: 'Email Already Registered', data: [] });
                }
                else {
                    bcrypt.hash(password, 10, (err, hash) => {
                        let userObj = {
                            email,
                            password: hash,
                        };
                        let promise = Librarian.create(userObj);
                        promise.then(
                            (resp) => {
                                payload = { id: resp.id };
                                let token = jwt.sign(
                                    payload,
                                    strategies.jwtOptions.secretOrKey
                                );
                                payload["token"] = token;
                                return res.status(200).send({ success: true, message: 'Account Created', data: [{ token, resp }] });
                            })
                    })
                }
            });
        } catch (error) {
            throw error
        }
    },

    /*
    variables used:
    
    email: user email
    password: user password
    */
    loginLibrarian: async (req, res) => {
        var { email, password } = req.body;

        if (email == null || !email) {
            res.status(422).json({ success: false, message: 'Invalid email', data: [] });
            return
        }
        if (password == null || !password) {
            res.status(422).json({ success: false, message: 'Invalid password', data: [] });
            return
        }

        email = email.toLowerCase();

        Librarian.findOne({ email: email }).then(
            (dbUser) => {
                if (dbUser && bcrypt.compareSync(password, dbUser.password)) {
                    payload = { id: dbUser.id };
                    let token = jwt.sign(payload, strategies.jwtOptions.secretOrKey);
                    payload["token"] = token;
                    return res.status(200).json({ success: true, message: 'Login Successful', data: { payload, dbUser } });
                } else {
                    return res.status(401).json({ success: false, message: 'Email or Password is incorrect', data: [] });
                }
            }).catch(err => {
                if (err) {
                    console.log(err);
                }
            });
    }
}
