var Book = require('../../models/book.js');
var ObjectID = require('mongodb').ObjectId;
var moment = require('moment-business-days');
const mongoose = require('mongoose');

module.exports = {
    /*  
 
    variables used:            

    title:title of the book
    isbn:isbn of the book        
    year: year of publishing
    price: cover price
    */
    createBook: async (req, res) => {
        var { title, isbn, year, price } = req.body;

        if (title == null || !title) {
            res.status(422).json({ success: false, message: 'Invalid title', data: [] });
            return
        }
        if (isbn == null || !isbn) {
            res.status(422).json({ success: false, message: 'Invalid isbn', data: [] });
            return
        }
        if (year == null || !year) {
            res.status(422).json({ success: false, message: 'Invalid year', data: [] });
            return
        }
        if (price == null || !price) {
            res.status(422).json({ success: false, message: 'Invalid price', data: [] });
            return
        }
        try {
            let userObj = {
                title, isbn, year, price
            };
            let promise = Book.create(userObj);
            promise.then(
                (resp) => {
                    return res.status(200).send({ success: true, message: 'Book Created', data: [resp] });
                })
        } catch (error) {
            throw error
        }
    },
    /*

    no variables used
    
    */
    ListAllBooks: async (req, res) => {

        try {
            Book.find({}, (err, books) => {
                if (err) res.status(400).send();
                if (books.length < 1) {
                    res.status(404).json({ success: false, message: 'No books found', data: [] });
                } else {
                    res.status(200).json({ success: true, message: 'books found', data: books });
                }
            })
        } catch (error) {
            throw error
        }
    },
    /*  
 
    variables used:            

    id:id of the book
    
    */
    ListSpecificBook: async (req, res) => {
        var { id } = req.query;
        if (id == null || !id) {
            res.status(422).json({ success: false, message: 'Invalid id', data: [] });
            return
        }
        try {
            Book.findOne({ _id: id }, (err, books) => {
                if (err) res.status(400).send();
                if (!books) {
                    return res.status(404).json({ success: false, message: 'No book found', data: [] });
                } else {
                    books.CheckOut.slice(-1).pop()
                    var checkOut = books.CheckOut.slice(-1).pop()
                    return res.status(200).json({ success: true, message: 'books found', data: [{ books, checkOut }] });
                }
            })
        } catch (error) {
            throw error
        }
    },
    /*  
 
   variables used:            

   id:id of the book
   name: name of the borrower
   mobileNumber: mobile Number of the borrower
   IDcardNumber: ID card Number of the borrower
   returnDate: return date of book
   
   */
    checkoutBook: async (req, res) => {
        var { id, name, mobileNumber, IDcardNumber } = req.body;
        if (id == null || !id) {
            res.status(422).json({ success: false, message: 'Invalid id', data: [] });
            return
        }
        if (name == null || !name) {
            res.status(422).json({ success: false, message: 'Invalid name', data: [] });
            return
        }
        if (mobileNumber == null || !mobileNumber) {
            res.status(422).json({ success: false, message: 'Invalid mobileNumber', data: [] });
            return
        }
        if (IDcardNumber == null || !IDcardNumber) {
            res.status(422).json({ success: false, message: 'Invalid IDcardNumber', data: [] });
            return
        }

        try {
            const date = new Date();
            borrowDate = date
            var returnDate = moment(date, 'YYYY-MM-DD').businessAdd(15)._d
            let userObj = {
                id: new ObjectID(), name, mobileNumber, IDcardNumber, actualReturnDate: null, penalty: 0, borrowDate, returnDate
            };

            Book.findOneAndUpdate({ _id: id }, { CheckIn: false, $push: { CheckOut: userObj } }, (err, books) => {
                if (err) res.status(400).send();
                Book.findOne({ _id: id }, (err, books) => {
                    if (err) res.status(400).send();
                    if (!books) {
                        res.status(404).json({ success: false, message: 'No book found', data: [] });
                    } else {
                        res.status(200).json({ success: true, message: 'Checkout Done', data: books });
                    }
                })
            })
        } catch (error) {
            throw error
        }
    },
    /*  
 
  variables used:            

  id:id of the bookunique id of the borrower in the array
  penalty: penalty on the late return
     
  */
    checkInBook: async (req, res) => {
        var { id } = req.body;
        if (id == null || !id) {
            res.status(422).json({ success: false, message: 'Invalid id', data: [] });
            return
        }
        try {
            const date = new Date();
            actualReturnDate = date
            Book.findOne({ _id: id }, (err, books) => {
                if (err) res.status(400).send();
                if (!books) {
                    return res.status(404).json({ success: false, message: 'No book found', data: [] });
                } else {
                    var checkOut = books.CheckOut.slice(-1).pop()
                    var diff = moment(checkOut.actualReturnDate, 'YYYY-MM-DD').businessDiff(moment(actualReturnDate, 'YYYY-MM-DD'));
                    var penalty = diff * 5
                    Book.findOneAndUpdate({ _id: id, CheckOut: { $elemMatch: { id: ObjectID(checkOut.id) } } }, { CheckIn: true, $set: { "CheckOut.$.actualReturnDate": actualReturnDate, "CheckOut.$.penalty": penalty } }, (err, books) => {
                        if (err) res.status(400).send();
                        res.status(200).json({ success: true, message: 'books found', data: books });
                    })
                }
            })
        } catch (error) {
            throw error
        }
    },
}