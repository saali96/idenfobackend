const passportJWT = require('passport-jwt');
var Librarian = require('../models/librarian.js');
const bcrypt = require('bcryptjs');
const { ExtractJwt, Strategy } = passportJWT;
const jwtOptions = { jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken() };
jwtOptions.secretOrKey = '6D6u2D3e36';
const express = require("express");
const dotenv = require("dotenv");
const passport = require('passport');

const app = express();
app.use(passport.initialize());

const JWTStrategyLibrarian = new Strategy(jwtOptions, (jwt_payload, done) => {

    Librarian.findOne({ _id: jwt_payload.id }, (err, user) => {
        if (err) {
            return done(err, false);
        }
        if (user) {
            return done(null, user);
        }
        else {
            return done(null, false);
        }
    });
});



module.exports = {
    jwtOptions, JWTStrategyLibrarian
};
