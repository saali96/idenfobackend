const dotenv = require('dotenv')
const mongoose = require('mongoose');

const express = require("express");
const app = express();
app.set("env", "development");
const env = app.get("env");
dotenv.config({
    path: `${__dirname}/.env.${env}`,
});
mongoose.connect(process.env.CONN_URL, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
    if (err) throw err;
    console.log("db connected");
})
const db = mongoose.connection

db.on('error', function () { console.log('Cannot Connect to DB') })
db.once('open', function () { return; })
console.log(process.env.CONN_URL)

dotenv.config();
