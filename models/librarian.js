const mongoose = require('mongoose');
const schema = new mongoose.Schema({
    email: {
        type: String,
    },
    password: {
        type: String,
    }

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });


// Ensure virtual fields are serialised.
schema.set('toJSON', {
    virtuals: true
});

const librarianModel = mongoose.model('librarian', schema);

module.exports = librarianModel;










