const mongoose = require('mongoose');
const schema = new mongoose.Schema({
    title: {
        type: String,
    },
    isbn: {
        type: String,
    },
    year: {
        type: String,
    },
    price: {
        type: Number,
    },
    CheckIn: {
        type: Boolean,
        default: true
    },
    CheckOut: {
        type: Array,
        default: []
    },

}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });


// Ensure virtual fields are serialised.
schema.set('toJSON', {
    virtuals: true
});

const bookModel = mongoose.model('book', schema);

module.exports = bookModel;










